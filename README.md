# XGI-DATA has been moved to [Zenodo](https://zenodo.org/communities/xgi) and [Github](https://github.com/xgi-org/xgi-data)!

Copies of the data files will remain here for some time, but will not be updated or supported.
